from main_pi import *
def etaPlot():
    eps = 0.1
    s1Random = rn.Random()
    s1Random.seed()

    costhRandom = rn.Random()
    costhRandom.seed()

    t2Random = rn.Random()
    t2Random.seed()

    phiRandom = rn.Random()
    phiRandom.seed()

    Kfile = open("eta191-0110-I-II.dat", "w")

    s1min = (mpi + meta) ** 2 + 0.00001
    s1max = 4.5 ** 2

    s1count = round((s1max - s1min) / eps)
    costhcount = round(2 / eps)
    i = 1

    while i <= s1count:
        print(i, "/", s1count)
        i += 1
        s1 = s1Random.uniform(s1min, s1max)
        j = 1
        while j <= costhcount:
            j += 1
            costh = costhRandom.uniform(-1, 1)

            # t2mi = t2min(s, s1)
            t2mi = -1.0
            # t2ma = t2max(s, s1)
            t2ma = -0.1

            t2count = int((t2ma - t2mi) / eps)

            k = 1
            while k <= t2count:
                k += 1
                t2 = t2Random.uniform(t2mi, t2ma)

                phicount = int(2 * np.pi / eps)
                l = 1
                while l <= phicount:
                    l += 1
                    phi = phiRandom.uniform(0, 2 * np.pi)
                    alfa_prim = 0.9
                    p1 = genketa(s, s1, t2, costh, phi)
                    p2 = genkpi(s, s1, t2, costh, phi)
                    p3 = genp2(s, s1, t2, costh, phi)
                    s2 = km.scal_prod_4d(p2 + p3, p2 + p3)
                    setap = km.scal_prod_4d(p1 + p3, p1 + p3)
                    etaI = s / (alfa_prim * s1 * s2)
                    etaII = s / (alfa_prim * s1 * setap)

                    Kfile.write(
                        "{:04.3f}   {:04.3f}   {:04.3f}    {:07.3f}    {:07.3f}\n".format(np.sqrt(s1), np.sqrt(s2),
                                                                                          np.sqrt(setap), etaI, etaII))
    Kfile.close()


def s2_sect_plot(s, s1, t2):
    f = open("speta-t2="+str(t2)+"-s1="+str(s1)+".dat", "w")

    f.write("#s={:7.2}, s1={:7.2}, t2={:7.2}\n".format(s, s1, t2))
    for costh in np.arange(-1, 1, 0.01):
        s20 = s2fun(s, s1, t2, costh, 0)
        s2pi4 = s2fun(s, s1, t2, costh, np.pi / 4)
        s2pi2 = s2fun(s, s1, t2, costh, np.pi / 2)
        s234pi = s2fun(s, s1, t2, costh, np.pi * 3 / 4)
        s2pi = s2fun(s, s1, t2, costh, np.pi)
        s254pi = s2fun(s, s1, t2, costh, np.pi * 5 / 4)
        s232pi = s2fun(s, s1, t2, costh, np.pi * 3 / 2)
        s274pi = s2fun(s, s1, t2, costh, np.pi * 7 / 4)
        eta = s / (s1 * s20)
        # t1 = km.scal_prod_4d()
        f.write(
            "{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}\n".format(costh, s20, s2pi4, s2pi2, s234pi,
                                                                                       s2pi, s254pi, s232pi, s274pi))
        print("costh=", costh)

    f.close()


def speta_sect_plot(s, s1, t2):
    f = open("speta-t2="+str(t2)+"-s1="+str(s1)+".dat", "w")

    f.write("#s={:7.2}, s1={:7.2}, t2={:7.2}\n".format(s, s1, t2))
    for costh in np.arange(-1, 1, 0.01):
        s20 = spetafun(s, s1, t2, costh, 0)
        s2pi4 = spetafun(s, s1, t2, costh, np.pi / 4)
        s2pi2 = spetafun(s, s1, t2, costh, np.pi / 2)
        s234pi = spetafun(s, s1, t2, costh, np.pi * 3 / 4)
        s2pi = spetafun(s, s1, t2, costh, np.pi)
        s254pi = spetafun(s, s1, t2, costh, np.pi * 5 / 4)
        s232pi = spetafun(s, s1, t2, costh, np.pi * 3 / 2)
        s274pi = spetafun(s, s1, t2, costh, np.pi * 7 / 4)
        eta = s / (s1 * s20)
        # t1 = km.scal_prod_4d()
        f.write(
            "{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}\n".format(costh, s20, s2pi4, s2pi2, s234pi,
                                                                                       s2pi, s254pi, s232pi, s274pi))
        print("costh=", costh)

    f.close()


def etas_plot(s, s1, t2):
    alfaprim = 0.9
    f = open("etas.dat", "w")

    f.write("#s={:7.2}, s1={:7.2}, t2={:7.2}\n".format(s, s1, t2))
    for costh in np.arange(-1, 1, 0.01):
        s2 = s2fun(s, s1, t2, costh, np.pi / 2)
        spieta = spetafun(s, s1, t2, costh, np.pi / 2)
        etaI = s / (alfaprim * s1 * s2)
        etaII = s / (alfaprim * s1 * spieta)
        t1 = t1fun(s, s1, t2, costh, np.pi / 2)
        tpi = tpifun(s, s1, t2, costh, np.pi / 2)
        alfa1_I = alfa_a2(t1, mro)
        alfa2_I = alfa_pom(t2)

        alfa1_II = alfa_f2(tpi)
        alfa2_II = alfa_pom(t2)

        FI = sp.hyp1f1(1 - alfa1_I, 1 - alfa1_I + alfa2_I, -1 / etaI)
        FII = sp.hyp1f1(1 - alfa1_II, 1 - alfa1_II + alfa2_II, -1 / etaII)
        f.write(
            "{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.4f}{:9.4f}\n".format(costh, etaI, etaII, spieta, FI, FII))
        print("costh=", costh)

    f.close()


def alfa1alfa2plot(s, s1, t2=-0.55):
    f = open("alfa1alfa2.dat", "w")
    for costh in np.arange(-1, 1, 0.01):
        t1 = t1fun(s, s1, t2, costh)
        alfa1I = alfa_a2(t1, mro)
        alfa2I = alfa_pom(t2)
        f.write("{:9.2f}  {:9.2f}\n".format(costh, alfa1I - alfa2I))
        print(costh)

    f.close()


def plotV1V2(s, s1, t2):
    phi = np.pi / 4
    alfaprime = 0.9
    f = open("V1V2.dat", "w")
    for t1 in np.arange(-2, -0.1, 0.01):
        costh = (t1 - mpi ** 2 - meta ** 2 + (s1 - t2 + mpi ** 2) * (s1 + meta ** 2 - mpi ** 2) / (2 * s1)) / (
                km.lbd(s1, t2, mpi ** 2) * km.lbd(s1, meta ** 2, mpi ** 2) / (2 * s1))
        s2v = s2fun(s, s1, t2, costh, phi)
        eta = s / (alfaprime * s1 * s2v)
        alf1 = alfa_a2(t1, mro)
        alf2 = alfa_pom(t2)
        T1 = cpower(-alfaprime * s, alf1 - 1) * cpower(-alfaprime * s2v, alf2 - alf1) * V_1(eta, alf1, alf2)
        T2 = cpower(-alfaprime * s, alf2 - 1) * cpower(-alfaprime * s1, alf1 - alf2) * V_2(eta, alf1, alf2)
        f.write(
            "{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}{:9.2f}\n".format(t1, alf1 - alf2, T1.real, T1.imag, T2.real, T2.imag))

        print("t1=", t1, "costh=", costh)

def hiper_s2_plot():
    global s
    t2=-0.15
    costh=0
    f=open("hyper-vs-s2.dat","w")
    for m1 in np.arange(mpi+meta+.001,4,0.01):
        s1=m1**2
        setap=spetafun(s, s1, t2, costh, np.pi / 4)
        f.write("{:9.2f}{:9.2f}\n".format(s1,sp.hyp1f1(1.5,1.5,s/(0.9*s1*setap))))

def t2tpiplot():
    global s
    M1=2
    f=open("t1tpi-M1="+str(M1)+".dat","w")
    s1 = (M1) ** 2
    t2=-0.15
    phi=np.pi/4
    for costh in np.arange(-1,1,0.01):
        pa = genq1(s, s1, t2, costh, phi)
        pb = genp1(s, s1, t2, costh, phi)
        p1 = genketa(s, s1, t2, costh, phi)
        p2 = genkpi(s, s1, t2, costh, phi)
        p3 = genp2(s, s1, t2, costh, phi)
        t1 = km.scal_prod_4d(pa - p1, pa - p1)
        tpi=km.scal_prod_4d(pa-p2,pa-p2)
        print(costh)
        f.write("{:10.4}{:10.3}{:10.3}\n".format(costh,t1,tpi))

def plotGamGamIplGamGamI(s,s1,t2,phi):
    alfa_prim=0.9
    f=open("gammasIplgammasII.dat","w")
    for costh in np.arange(-1,1,0.001):
        pa = genq1(s, s1, t2, costh, phi)
        pb = genp1(s, s1, t2, costh, phi)
        p1 = genketa(s, s1, t2, costh, phi)
        p2 = genkpi(s, s1, t2, costh, phi)
        p3 = genp2(s, s1, t2, costh, phi)
        speta = km.scal_prod_4d(p1 + p3, p1 + p3)
        s2 = km.scal_prod_4d(p2 + p3, p2 + p3)
        t1 = km.scal_prod_4d(pa - p1, pa - p1)
        tpi = km.scal_prod_4d(pa - p2, pa - p2)
        KI = Kexplicit(pa, pb, p1, p3)
        KII = Kexplicit(pa, pb, p2, p3)

        etaI = s / (alfa_prim * s1 * s2)
        etaII = s / (alfa_prim * s1 * speta)

        alfa1I = alfa_a2(t1, mro)
        alfa2I = alfa_pom(t2)

        alfa1II = alfa_f2(tpi)
        alfa2II = alfa_pom(t2)

        a1I = cpower(-alfa_prim * s, alfa1I - 1) * cpower(-alfa_prim * s2, alfa2I - alfa1I)
        a2I = cpower(-alfa_prim * s, alfa2I - 1) * cpower(-alfa_prim * s1, alfa1I - alfa2I)

        a1II = cpower(-alfa_prim * s, alfa1II - 1) * cpower(-alfa_prim * speta, alfa2II - alfa1II)
        a2II = cpower(-alfa_prim * s, alfa2II - 1) * cpower(-alfa_prim * s1, alfa1II - alfa2II)

        val=abs(sp.gamma(1-alfa1I)*sp.gamma(1-alfa2I)*(a1I*V_1(etaI,alfa1I,alfa2I)+a2I*V_2(etaI,alfa1I,alfa2I))+
                sp.gamma(1-alfa1II)*sp.gamma(1-alfa2II)*(a1II*V_1(etaII,alfa1II,alfa2II)+a2II*V_2(etaII,alfa1II,alfa2II)))
        f.write("{:9.3}{:9.5}\n".format(costh,val))
        print(costh)

def a2pomt1PoleLine(k,t2):
    return (-k+(1.08-0.47)+.25*t2)/0.9

def plott1PoleLine():
    f=open("t1t2poles.dat","w")
    for t2 in np.arange(-4,4,0.1):
        t10=a2pomt1PoleLine(0,t2)
        t11=a2pomt1PoleLine(1,t2)
        t12=a2pomt1PoleLine(2,t2)
        t13=a2pomt1PoleLine(3,t2)
        f.write("{:10.3}{:10.3}{:10.3}{:10.3}{:10.3}\n".format(t2,t10,t11,t12,t13))

def TIplT2sq(phi,s,s1,t2,costh):
    for costh in np.arange(-1, 1, 0.001):
        print(costh)
        val = []
        mmin = 0.8
        # s1=4*2
        for i in range(0, 9):
            s1 = (mmin + i*0.4) ** 2
            pa = genq1(s, s1, t2, costh, phi)
            pb = genp1(s, s1, t2, costh, phi)
            p1 = genketa(s, s1, t2, costh, phi)
            p2 = genkpi(s, s1, t2, costh, phi)
            p3 = genp2(s, s1, t2, costh, phi)
            speta=km.scal_prod_4d(p1+p3,p1+p3)
            s2 = km.scal_prod_4d(p2 + p3, p2 + p3)
            t1 = km.scal_prod_4d(pa - p1, pa - p1)
            tpi=km.scal_prod_4d(pa-p2,pa-p2)
            KI = Kexplicit(pa, pb, p1, p3)
            KII = Kexplicit(pa, pb, p2, p3)

            etaI = s / (alfa_prim * s1 * s2)
            etaII= s/(alfa_prim * s1 * speta)

            alfa1I=alfa_a2(t1,mro)
            alfa2I = alfa_pom(t2)

            alfa1II=alfa_f2(tpi)
            alfa2II = alfa_pom(t2)

            a1I = cpower(-alfa_prim * s, alfa1I - 2) * cpower(-alfa_prim * s2, alfa2I - alfa1I)
            a2I = cpower(-alfa_prim * s, alfa2I - 2) * cpower(-alfa_prim * s1, alfa1I - alfa2I)

            a1II = cpower(-alfa_prim * s, alfa1II - 2) * cpower(-alfa_prim * speta, alfa2II - alfa1II)
            a2II = cpower(-alfa_prim * s, alfa2II - 2) * cpower(-alfa_prim * s1, alfa1II - alfa2II)


            V1I = V_1(etaI, alfa1I, alfa2I)
            V2I = V_2(etaI, alfa1I, alfa2I)

            V1II = V_1(etaII, alfa1II, alfa2II)
            V2II = V_2(etaII, alfa1II, alfa2II)

            TItilde=sp.gamma(2-alfa1I)*sp.gamma(2-alfa2I)*(V1I*a1I + V2I*a2I)*KI
            TIItilde=sp.gamma(2-alfa1II)*sp.gamma(2-alfa2II)*(V1II*a1II + V2II*a2II)*KII

            val.append(abs(TItilde+TIItilde) ** 2)
