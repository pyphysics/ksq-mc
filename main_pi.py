import numpy as np
from sympy.polys.domains import mpelements

import pylblibs.kinematics as km
import pylblibs.particleproperties as pp
from sympy import Eijk, N
import random as rn
import vegas
import scipy.special as sp
import warnings
import cmath
import pylblibs.numerical as num
import scipy.integrate as integr

# from main_gam import KSQs1costh

mp = pp.m_proton
mpi = pp.m_pion
meta = pp.m_eta
mro = pp.m_ro
ma2 = pp.m_a2
# meta = pp.m_etaprim

Epi = 191
s = mp ** 2 + mpi ** 2 + 2 * mp * Epi


def cpower(a, b):
    (r, phi) = cmath.polar(a)
    r = float(r)
    phi = float(phi)
    return np.power(r, b) * np.exp(1j * phi * b)


def genq1(s, s1, t2, costh, phi):
    Eq = (s1 - t2 + mpi ** 2) / (2 * np.sqrt(s1))
    qmod = np.sqrt(Eq ** 2 - mpi ** 2)
    return np.array([Eq, 0, 0, qmod])


def genp2(s, s1, t2, costh, phi):
    E2 = (s - s1 - mp ** 2) / (2 * np.sqrt(s1))
    p2mod = np.sqrt(E2 ** 2 - mp ** 2)
    costh2 = (2 * s1 * (mp ** 2 + s1 - s - t2) + (s1 - t2 + mpi ** 2) * (s - s1 - mp ** 2)) / (
            np.sqrt(km.lbd(t2, s1, mpi ** 2)) * np.sqrt(km.lbd(s1, s, mp ** 2)))
    if (np.abs(costh2) > 1):
        print("costh2=", costh2)
    sinth2 = np.sqrt(1 - costh2 ** 2)
    return np.array([E2, p2mod * sinth2, 0, p2mod * costh2])


def genkpi(s, s1, t2, costh, phi):
    kmod = np.sqrt(km.lbd(s1, mpi ** 2, meta ** 2) / (4 * s1))
    Epi = np.sqrt(kmod ** 2 + mpi ** 2)
    sinth = np.sqrt(1 - costh ** 2)
    return np.array([Epi, -kmod * sinth * np.cos(phi), -kmod * sinth * np.sin(phi), -kmod * costh])


def genketa(s, s1, t2, costh, phi):
    Eeta = (s1 - mpi ** 2 + meta ** 2) / (2 * np.sqrt(s1))
    keta = -genkpi(s, s1, t2, costh, phi)
    keta[0] = Eeta
    return keta


def genp1(s, s1, t2, costh, phi):
    return genkpi(s, s1, t2, costh, phi) + genketa(s, s1, t2, costh, phi) + genp2(s, s1, t2, costh, phi) - genq1(s, s1,
                                                                                                                 t2,
                                                                                                                 costh,
                                                                                                                 phi)


def K(pa, pb, p1, p3):
    val = 0
    for mu in range(0, 4):
        for nu in range(0, 4):
            for ro in range(0, 4):
                for si in range(0, 4):
                    val = val + N(Eijk(mu, nu, ro, si)) * (p3[mu] - pb[mu]) * (p3[nu] + pb[nu]) * (pa[ro] - p1[ro]) * (
                            pa[si] + p1[si])

    return val


def Kexplicit(pa, pb, p1, p3):
    p2 = pa + pb - p1 - p3
    s1 = km.scal_prod_4d(p1 + p2, p1 + p2)
    pa3 = km.v4tov3(pa)
    p13 = km.v4tov3(p1)
    p33 = km.v4tov3(p3)
    pa3mod = np.linalg.norm(pa3)
    p33mod = np.linalg.norm(p33)
    p13mod = np.linalg.norm(p13)
    sinth1sinphi1 = p13[1] / p13mod
    sinkappa = p33[0] / p33mod
    return 4 * np.sqrt(s1) * pa3mod * p33mod * p13mod * sinkappa * sinth1sinphi1


def t2max(s, s1):
    kpietamod = np.sqrt(km.lbd(s, s1, mp ** 2) / (4 * s))
    qmod = np.sqrt(km.lbd(s, 0, mp ** 2) / (4 * s))
    Epieta = np.sqrt(kpietamod ** 2 + s1)
    return s1 - 2 * Epieta * qmod + 2 * qmod * kpietamod


def t2min(s, s1):
    kpietamod = np.sqrt(km.lbd(s, s1, mp ** 2) / (4 * s))
    qmod = np.sqrt(km.lbd(s, 0, mp ** 2) / (4 * s))
    Epieta = np.sqrt(kpietamod ** 2 + s1)
    return s1 - 2 * Epieta * qmod - 2 * qmod * kpietamod


def KSQ(t2phi):
    global costh, s1, s
    t2 = t2phi[0]
    phi = t2phi[1]
    q1 = genq1(s, s1, t2, costh, phi)
    p1 = genp1(s, s1, t2, costh, phi)
    keta = genketa(s, s1, t2, costh, phi)
    p2 = genp2(s, s1, t2, costh, phi)
    return Kexplicit(q1, p1, keta, p2) ** 2


def tmingen(s, m1, m2, m3, m4):
    return m1 ** 2 + m3 ** 2 - (s - m2 ** 2 + m1 ** 2) * (s - m4 ** 2 + m3 ** 2) / (2 * s) - np.sqrt(
        km.lbd(s, m1 ** 2, m2 ** 2) * km.lbd(s, m3 ** 2, m4 ** 2)) / (2 * s)


def tmaxgen(s, m1, m2, m3, m4):
    return m1 ** 2 + m3 ** 2 - (s - m2 ** 2 + m1 ** 2) * (s - m4 ** 2 + m3 ** 2) / (2 * s) + np.sqrt(
        km.lbd(s, m1 ** 2, m2 ** 2) * km.lbd(s, m3 ** 2, m4 ** 2)) / (2 * s)


def alfa_v(t, mv):
    return 1.0 + 0.9 * (t - mv ** 2)


def alfa_pom(t):
    return 1.08 + 0.25 * t


def alfa_a2(t, ma2):
    return alfa_v(t, mro)


def alfa_f2(t):
    return 0.47 + 0.89 * t


def V_1(eta, alf1, alf2):
    beta0 = 1
    return beta0 * sp.gamma(alf1 - alf2) / sp.gamma(1 - alf2) * sp.hyp1f1(1 - alf1, 1 - alf1 + alf2, -1 / eta)
    # return beta0 * sp.gamma(alf1 - alf2) / sp.gamma(1 - alf2) * num.hyp1F1(1 - alf1, 1 - alf1 + alf2, -1 / eta)


# def V_1approx(eta, alf1, alf2):
#     return 1 / (sp.gamma(-alf2) * (alf1 - alf2))


def V_2(eta, alf1, alf2):
    beta0 = 1
    # return beta0 * sp.gamma(alf2 - alf1) / sp.gamma(1- alf1) * num.hyp1F1(1-alf2, 1 - alf2 + alf1, -1 / eta)
    return beta0 * sp.gamma(alf2 - alf1) / sp.gamma(1 - alf1) * sp.hyp1f1(1 - alf2, 1 - alf2 + alf1, -1 / eta)


# def V_2approx(eta, alf1, alf2):
#     return 1 / (sp.gamma(-alf1) * (alf2 - alf1))

def ksi_i(taui, alfai):
    return 0.5 * (taui + sp.exp(-np.pi * alfai * 1j))


def ksi_ij(taui, tauj, alfai, alfaj):
    return 0.5 * (taui * tauj + sp.exp(-np.pi * (alfai - alfaj) * 1j))


def costheta2(s, s1, t2):
    return (2 * s1 * (mp ** 2 + s1 - s - t2) + (s1 + mpi ** 2) * (s - s1 - mp ** 2)) / (
        np.sqrt(km.lbd(s1, t2, mpi ** 2) * km.lbd(s1, s, mp ** 2)))


def sintheta2(s, s1, t2):
    return np.sqrt(1 - costheta2(s, s1, t2) ** 2)


def s2fun(s, s1, t2, costh, phi):
    sinth = np.sqrt(1 - costh ** 2)
    return mp ** 2 + mpi ** 2 + 1 / (2 * s1) * ((s1 + mpi ** 2 - meta ** 2) * (s - s1 - mp ** 2) + np.sqrt(
        km.lbd(s1, mpi ** 2, meta ** 2) * km.lbd(s1, s, mp ** 2)) * (
                                                        sintheta2(s, s1, t2) * sinth * np.cos(phi) + costheta2(s,
                                                                                                               s1,
                                                                                                               t2) * costh))


def spetafun(s, s1, t2, costh, phi):
    keta = genketa(s, s1, t2, costh, phi)
    p2 = genp2(s, s1, t2, costh, phi)
    return km.scal_prod_4d(keta + p2, keta + p2)


def Ta2pom(s, s1, t2, costh, phi):
    alfa_prim = 0.9
    pa = genq1(s, s1, t2, costh, phi)
    pb = genp1(s, s1, t2, costh, phi)
    p1 = genketa(s, s1, t2, costh, phi)
    p2 = genkpi(s, s1, t2, costh, phi)
    p3 = genp2(s, s1, t2, costh, phi)
    s2 = km.scal_prod_4d(p2 + p3, p2 + p3)
    t1 = km.scal_prod_4d(pa - p1, pa - p1)
    eta = s / (alfa_prim * s1 * s2)

    alfa1 = alfa_a2(t1, mro)
    alfa2 = alfa_pom(t2)
    K = Kexplicit(pa, pb, p1, p3)
    V1 = V_1(eta, alfa1, alfa2)
    V2 = V_2(eta, alfa1, alfa2)
    Gam1 = sp.gamma(1 - alfa1)
    # Gam1 = 1
    Gam2 = sp.gamma(1 - alfa2)
    # Gam2 = 1
    a1 = cpower((s / s2), (alfa1 - 1.0)) * cpower((-alfa_prim * s2), (alfa2 - 1.0))
    a2 = cpower(-alfa_prim * s, alfa2 - 1) * cpower(-alfa_prim * s1, alfa1 - alfa2)
    # a1 = 1
    # a2 = 1
    val = K * Gam1 * Gam2 * (a1 * V1 + a2 * V2)
    return val


def Tf2pom(s, s1, t2, costh, phi):
    alfa_prim = 0.9
    pa = genq1(s, s1, t2, costh, phi)
    pb = genp1(s, s1, t2, costh, phi)
    p1 = genketa(s, s1, t2, costh, phi)
    p2 = genkpi(s, s1, t2, costh, phi)
    p3 = genp2(s, s1, t2, costh, phi)

    tpi = t2 + meta ** 2 + 2 * km.scal_prod_4d(p1, p3) - 2 * km.scal_prod_4d(p1, pb)
    spieta = km.scal_prod_4d(p1 + p3, p1 + p3)
    s2 = spieta
    eta = s / (alfa_prim * s1 * s2)

    alfa1 = alfa_f2(tpi)
    alfa2 = alfa_pom(t2)
    K = Kexplicit(pa, pb, p2, p3)  # here p1 is replaced with p2 eta <-->pi in upper vertex
    V1 = V_1(eta, alfa1, alfa2)
    V2 = V_2(eta, alfa1, alfa2)
    # V1 = 1
    # V2 = 1
    Gam1 = sp.gamma(1 - alfa1)
    Gam2 = sp.gamma(1 - alfa2)
    a1 = cpower((s / s2), (alfa1 - 1.0)) * cpower((-alfa_prim * s2), (alfa2 - 1.0))
    a2 = cpower(-alfa_prim * s, alfa2 - 1) * cpower(-alfa_prim * s1, alfa1 - alfa2)
    val = K * Gam1 * Gam2 * (a1 * V1 + a2 * V2)
    return val


def TSQ(t2phi):
    global costh, s1, s
    t2 = t2phi[0]
    phi = t2phi[1]
    # return abs(Ta2pom(s,s1,t2,costh,phi)+Tf2pom(s,s1,t2,costh,phi))  ** 2
    return abs(Tf2pom(s, s1, t2, costh, phi) + Ta2pom(s, s1, t2, costh, phi)) ** 2


def diffdistrib(t2phi):
    global costh, s1, s
    t2 = t2phi[0]
    phi = t2phi[1]
    pcm = np.sqrt(km.lbd(s, mp ** 2, mpi ** 2) / (4 * s))

    return km.conv / (4 * np.sqrt(s) * pcm) * np.pi / np.sqrt(km.lbd(s, mp ** 2, mpi ** 2)) * np.sqrt(
        km.lbd(s1, mpi ** 2, meta ** 2)) / (16 * np.sqrt(s1)) * 2 * np.sqrt(s1) \
           * abs(Tf2pom(s, s1, t2, costh, phi) + Ta2pom(s, s1, t2, costh, phi)) ** 2


# removing the 1/(2pi)^4
# 2*np.sqrt(s1) this is the jacobian of the s1 to Mpieta change

def dsig_dphi(phi, s, s1, costh, t2):
    phi = phi[0]
    pcm = np.sqrt(km.lbd(s, mp ** 2, mpi ** 2) / (4 * s))
    # val=\
    # km.conv / (4 * np.sqrt(s) * pcm) * np.pi / np.sqrt(km.lbd(s, mp ** 2, mpi ** 2)) * np.sqrt(km.lbd(s1, mpi ** 2, meta ** 2)) / (16 * np.sqrt(s1)) * 2 * np.sqrt(s1)* \
    # temporarily remove phase space +flux
    # val=  abs(Tf2pom(s, s1, t2, costh, phi) + Ta2pom(s, s1, t2, costh, phi)) ** 2
    val = abs(Ta2pom(s, s1, t2, costh, phi)) ** 2
    return val


def KSQcosthplot(s1x):
    KSQcosthfile = open("KSQcosth195-01-02-etapi3.0.dat", "w")
    KSQcosthfile.write("{:8s}{:12s}\n".format("#costh", "KSQ"))
    c = -1.0
    while (c <= 1.0):
        eps = 0.05
        if abs(abs(c) - 1.0) <= 0.1:
            eps = 0.01
        KSQcosthfile.write("{:8.3f}{:12.3f}\n".format(c, KSQs1costh(s1x, c)))
        print(c)
        c += eps
    KSQcosthfile.close()


def TSQplotNoMC():
    global s1, costh
    eps = 0.2

    Kfile = open("Ta2f2SQ-191-0110.dat", "w")

    s1min = (mpi + meta) ** 2 + 0.00001
    s1max = 4.5 ** 2

    s1count = round((s1max - s1min) / eps)
    costhcount = round(2 / eps)
    i = 0

    while i <= s1count:
        print(i, "/", s1count)
        s1 = s1min + i * eps
        i += 1
        j = 0
        while j <= costhcount:
            costh = -1 + j * eps
            j += 1
            # t2mi = t2min(s, s1)
            t2mi = -1.0
            # t2ma = t2max(s, s1)
            t2ma = -0.1

            integ = vegas.Integrator([[t2mi, t2ma], [0, 2 * np.pi]])
            result = integ(TSQ, nitn=10, neval=1000)

            Kfile.write("{:04.3f}   {:04.3f}   {:07.3f}\n".format(np.sqrt(s1), costh, result.mean))
            Kfile.flush()
    Kfile.close()


def t2plot(s, s1, t2):
    f = open("t1.dat", "w")
    for costh in np.arange(-1, 1, 0.01):
        pa = genq1(s, s1, t2, costh, 0)
        p1 = genketa(s, s1, t2, costh, 0)
        t1 = km.scal_prod_4d(pa - p1, pa - p1)
        f.write("{:04.3f}     {:04.3f}\n ".format(costh, t1))
        print(costh)


def t1fun(s, s1, t2, costh, phi):
    pa = genq1(s, s1, t2, costh, phi)
    p1 = genketa(s, s1, t2, costh, phi)
    return km.scal_prod_4d(pa - p1, pa - p1)


def tpifun(s, s1, t2, costh, phi):
    pa = genq1(s, s1, t2, costh, phi)
    p2 = genkpi(s, s1, t2, costh, phi)
    return km.scal_prod_4d(pa - p2, pa - p2)


def dsig_ds1dcosth():
    global s1, costh
    eps = 0.2
    # s1Random = rn.Random()
    # s1Random.seed()

    # costhRandom = rn.Random()
    # costhRandom.seed()

    Kfile = open("dsigdm-191-0110-035.dat", "w")

    s1min = (mpi + meta) ** 2 + 0.00001
    s1max = 3.5 ** 2

    s1count = round((s1max - s1min) / eps)
    costhcount = round(2 / eps)
    i = 0

    while i <= s1count:
        print(i, "/", s1count)
        s1 = s1min + i * eps
        i += 1
        j = 0
        while j <= costhcount:
            costh = -1 + j * eps
            j += 1
            # t2mi = t2min(s, s1)
            t2mi = -1.0
            # t2ma = t2max(s, s1)
            t2ma = -0.1

            integ = vegas.Integrator([[t2mi, t2ma], [0, 2 * np.pi]])
            result = integ(diffdistrib, nitn=5, neval=1000)

            Kfile.write("{:04.3f}   {:04.3f}   {:09.5f}\n".format(np.sqrt(s1), costh, result.mean))
            Kfile.flush()
    Kfile.close()


def dsig_ds1dcosth_fixedt2():
    t2 = -0.15
    eps = 0.1

    Kfile = open("dsigdm-191-015-a2sq.dat", "w")

    s1min = (mpi + meta) ** 2 + 0.00001
    s1max = 4.5 ** 2

    s1count = round((s1max - s1min) / eps)
    costhcount = round(2 / eps)
    i = 0
    warnings.filterwarnings("ignore")
    while i <= s1count:
        print(i, "/", s1count)
        s1 = s1min + i * eps
        i += 1
        j = 0
        while j <= costhcount:
            costh = -1 + j * eps
            j += 1

            result = integr.quadrature(dsig_dphi, 0.0, 2.0 * np.pi, args=(s, s1, costh, t2), tol=10 ** -2)

            Kfile.write("{:04.3f}   {:04.3f}   {:09.2f}\n".format(np.sqrt(s1), costh, result[0]))
    Kfile.close()


def TIplTII_costhplot():
    global s

    alfa_prim = 0.9
    t2 = -1.0
    f = open("T1+T2-front-factors-t2=" + str(t2) + "test.dat", "w")
    phi = np.pi / 4
    for costh in np.arange(-1, 1, 0.01):
        print(costh)
        val = []
        mmin = 0.8
        # s1=4*2
        for i in range(0, 9):
            s1 = (mmin + i * 0.4) ** 2
            pa = genq1(s, s1, t2, costh, phi)
            pb = genp1(s, s1, t2, costh, phi)
            p1 = genketa(s, s1, t2, costh, phi)
            p2 = genkpi(s, s1, t2, costh, phi)
            p3 = genp2(s, s1, t2, costh, phi)
            speta = km.scal_prod_4d(p1 + p3, p1 + p3)
            s2 = km.scal_prod_4d(p2 + p3, p2 + p3)
            t1 = km.scal_prod_4d(pa - p1, pa - p1)
            tpi = km.scal_prod_4d(pa - p2, pa - p2)
            KI = Kexplicit(pa, pb, p1, p3)
            KII = Kexplicit(pa, pb, p2, p3)

            etaI = s / (alfa_prim * s1 * s2)
            etaII = s / (alfa_prim * s1 * speta)

            alfa1I = alfa_a2(t1, mro)
            alfa2I = alfa_pom(t2)

            alfa1II = alfa_f2(tpi)
            alfa2II = alfa_pom(t2)

            a1I = cpower(-alfa_prim * s, alfa1I - 1) * cpower(-alfa_prim * s2, alfa2I - alfa1I)
            a2I = cpower(-alfa_prim * s, alfa2I - 1) * cpower(-alfa_prim * s1, alfa1I - alfa2I)

            a1II = cpower(-alfa_prim * s, alfa1II - 1) * cpower(-alfa_prim * speta, alfa2II - alfa1II)
            a2II = cpower(-alfa_prim * s, alfa2II - 1) * cpower(-alfa_prim * s1, alfa1II - alfa2II)

            # print("etaI=",etaI)
            # print("etaII=",etaII)

            V1I = V_1(etaI, alfa1I, alfa2I)
            V2I = V_2(etaI, alfa1I, alfa2I)

            V1II = V_1(etaII, alfa1II, alfa2II)
            V2II = V_2(etaII, alfa1II, alfa2II)

            # TItilde=sp.gamma(1-alfa1I)*sp.gamma(1-alfa2I)*(V1I*a1I + V2I*a2I)*(-alfa_prim*s2)
            # TIItilde=sp.gamma(1-alfa1II)*sp.gamma(1-alfa2II)*(V1II*a1II + V2II*a2II)*(-alfa_prim*speta)

            TItilde=sp.gamma(1-alfa1I)*sp.gamma(1-alfa2I)*(V1I*a1I + V2I*a2I)
            TIItilde=sp.gamma(1-alfa1II)*sp.gamma(1-alfa2II)*(V1II*a1II + V2II*a2II)


            # TItilde=sp.gamma(1-alfa1I)*sp.gamma(1-alfa2I)*(V1I*a1I + V2I*a2I)*(1-alfa2I)
            # TIItilde=sp.gamma(1-alfa1II)*sp.gamma(1-alfa2II)*(V1II*a1II + V2II*a2II)*(1-alfa2I)

            # TItilde = sp.gamma(1 - alfa1I) * sp.gamma(1 - alfa2I)
            # TIItilde = sp.gamma(1 - alfa1II) * sp.gamma(1 - alfa2II)

            val.append(abs(TItilde + TIItilde) ** 2)
        f.write(
            "{:11.3f}{:16.3f}{:16.3f}{:16.3f}{:16.3f}{:16.3f}{:16.3f}{:16.3f}{:16.3f}{:16.3f}\n".format(costh, val[0],
                                                                                                        val[1], val[2],
                                                                                                        val[3], val[4],
                                                                                                        val[5], val[6],
                                                                                                        val[7], val[8]))


def dsigt_eta(phi, s1, t2, teta):
    global s

    alfa_prim = 0.9
    costh=(2*s1*(teta-mpi**2-meta**2)+(s1-t2+mpi**2)*(s1+meta**2-mpi**2))/np.sqrt(km.lbd(s1,t2,mpi**2)*km.lbd(s1,meta**2,mpi**2))
    pa = genq1(s, s1, t2, costh, phi)
    pb = genp1(s, s1, t2, costh, phi)
    p1 = genketa(s, s1, t2, costh, phi)
    p2 = genkpi(s, s1, t2, costh, phi)
    p3 = genp2(s, s1, t2, costh, phi)
    speta = km.scal_prod_4d(p1 + p3, p1 + p3)
    s2 = km.scal_prod_4d(p2 + p3, p2 + p3)
    t1 = km.scal_prod_4d(pa - p1, pa - p1)
    tpi = km.scal_prod_4d(pa - p2, pa - p2)
    KI = Kexplicit(pa, pb, p1, p3)
    KII = Kexplicit(pa, pb, p2, p3)

    etaI = s / (alfa_prim * s1 * s2)
    etaII = s / (alfa_prim * s1 * speta)

    alfa1I = alfa_a2(t1, mro)
    alfa2I = alfa_pom(t2)

    alfa1II = alfa_f2(tpi)
    alfa2II = alfa_pom(t2)

    a1I = cpower(-alfa_prim * s, alfa1I - 1) * cpower(-alfa_prim * s2, alfa2I - alfa1I)
    a2I = cpower(-alfa_prim * s, alfa2I - 1) * cpower(-alfa_prim * s1, alfa1I - alfa2I)

    a1II = cpower(-alfa_prim * s, alfa1II - 1) * cpower(-alfa_prim * speta, alfa2II - alfa1II)
    a2II = cpower(-alfa_prim * s, alfa2II - 1) * cpower(-alfa_prim * s1, alfa1II - alfa2II)

    V1I = V_1(etaI, alfa1I, alfa2I)
    V2I = V_2(etaI, alfa1I, alfa2I)

    V1II = V_1(etaII, alfa1II, alfa2II)
    V2II = V_2(etaII, alfa1II, alfa2II)

    epsilon=0.1

    TItilde = sp.gamma(1 - alfa1I) * sp.gamma(1 - alfa2I-epsilon) * (V1I * a1I + V2I * a2I)*KI
    TIItilde = sp.gamma(1 - alfa1II) * sp.gamma(1 - alfa2II) * (V1II * a1II + V2II * a2II)*KII

    val=TIItilde+TIItilde
