import numpy as np
import pylblibs.kinematics as km
import pylblibs.particleproperties as pp
from sympy import Eijk, N
import random as rn
import vegas

mp = pp.m_proton
mpi = pp.m_pion
meta = pp.m_eta
# meta = pp.m_etaprim

Egam = 10
s = mp ** 2 + 2 * mp * Egam


def genq(s, s1, t2, costh, phi):
    qmod = (s1 - t2) / (2 * np.sqrt(s1))
    return np.array([qmod, 0, 0, qmod])


def genp2(s, s1, t2, costh, phi):
    E2 = (s - s1 - mp ** 2) / (2 * np.sqrt(s1))
    p2mod = np.sqrt(E2 ** 2 - mp ** 2)
    costh2 = (2 * s1 * (mp ** 2 + s1 - s - t2) + (s1 - t2) * (s - s1 - mp ** 2)) / (
            (s1 - t2) * np.sqrt(km.lbd(s1, s, mp ** 2)))
    sinth2 = np.sqrt(1 - costh2 ** 2)
    return np.array([E2, p2mod * sinth2, 0, p2mod * costh2])


def genkpi(s, s1, t2, costh, phi):
    kmod = np.sqrt(km.lbd(s1, mpi ** 2, meta ** 2) / (4 * s1))
    Epi = np.sqrt(kmod ** 2 + mpi ** 2)
    sinth = np.sqrt(1 - costh ** 2)
    return np.array([Epi, -kmod * sinth * np.cos(phi), -kmod * sinth * np.sin(phi), -kmod * costh])


def genketa(s, s1, t2, costh, phi):
    Eeta = (s1 - mpi ** 2 + meta ** 2) / (2 * np.sqrt(s1))
    keta = -genkpi(s, s1, t2, costh, phi)
    keta[0] = Eeta
    return keta


def genp1(s, s1, t2, costh, phi):
    return genkpi(s, s1, t2, costh, phi) + genketa(s, s1, t2, costh, phi) + genp2(s, s1, t2, costh, phi) - genq(s, s1,
                                                                                                                t2,
                                                                                                                costh,
                                                                                                                phi)


def K(pa, pb, p1, p3):
    val = 0
    for mu in range(0, 4):
        for nu in range(0, 4):
            for ro in range(0, 4):
                for si in range(0, 4):
                    val = val + N(Eijk(mu, nu, ro, si)) * (p3[mu] - pb[mu]) * (p3[nu] + pb[nu]) * (pa[ro] - p1[ro]) * (
                            pa[si] + p1[si])

    return val


def Kexplicit(pa, pb, p1, p3):
    p2 = pa + pb - p1 - p3
    s1 = km.scal_prod_4d(p1 + p2, p1 + p2)
    pa3 = km.v4tov3(pa)
    p13 = km.v4tov3(p1)
    p33 = km.v4tov3(p3)
    pa3mod = np.linalg.norm(pa3)
    p33mod = np.linalg.norm(p33)
    p13mod = np.linalg.norm(p13)
    sinth1sinphi1 = p13[1] / p13mod
    sinkappa = p33[0] / p33mod
    return 4 * np.sqrt(s1) * pa3mod * p33mod * p13mod * sinkappa * sinth1sinphi1


def t2max(s, s1):
    kpietamod = np.sqrt(km.lbd(s, s1, mp ** 2) / (4 * s))
    qmod = np.sqrt(km.lbd(s, 0, mp ** 2) / (4 * s))
    Epieta = np.sqrt(kpietamod ** 2 + s1)
    return s1 - 2 * Epieta * qmod + 2 * qmod * kpietamod


def t2min(s, s1):
    kpietamod = np.sqrt(km.lbd(s, s1, mp ** 2) / (4 * s))
    qmod = np.sqrt(km.lbd(s, 0, mp ** 2) / (4 * s))
    Epieta = np.sqrt(kpietamod ** 2 + s1)
    return s1 - 2 * Epieta * qmod - 2 * qmod * kpietamod


def KSQ(t2phi):
    global costh, s1, s
    t2 = t2phi[0]
    phi = t2phi[1]
    q = genq(s, s1, t2, costh, phi)
    p1 = genp1(s, s1, t2, costh, phi)
    keta = genketa(s, s1, t2, costh, phi)
    p2 = genp2(s, s1, t2, costh, phi)
    return Kexplicit(q, p1, keta, p2) ** 2


def KSQs1costh(s1x, costhx):
    global s1, costh
    s1 = s1x
    costh = costhx
    t2mi = -0.2
    t2ma = -0.1
    integ = vegas.Integrator([[t2mi, t2ma], [0, 2 * np.pi]])
    return integ(KSQ, nitn=10, neval=1000).mean


# def KSQcosthplot(s1x):
#     KSQcosthfile = open("KSQcosth195-01-02-etapi3.0.dat","w")
#     KSQcosthfile.write("{:8s}{:12s}\n".format("#costh", "KSQ"))
#     c=-1.0
#     while(c<=1.0):
#         eps=0.05
#         if abs(abs(c) - 1.0)<=0.1:
#             eps=0.01
#         KSQcosthfile.write("{:8.3f}{:12.3f}\n".format(c,KSQs1costh(s1x, c)))
#         print(c)
#         c+=eps
#     KSQcosthfile.close()


def fullt2plot():
    eps: float = 0.2
    s1Random = rn.Random()
    s1Random.seed()

    costhRandom = rn.Random()
    costhRandom.seed()

    Kfile = open("pieta195-vegas-full.dat", "w")

    s1min = (mpi + meta) ** 2
    s1max = 4.5 ** 2

    s1count = round((s1max - s1min) / eps)
    costhcount = round(2 / eps)
    i = 1

    while i <= s1count:
        print(i, "/", s1count)
        i += 1
        s1 = s1Random.uniform(s1min, s1max)
        j = 1
        while j <= costhcount:
            j += 1
            costh = costhRandom.uniform(-1, 1)

            t2mi = t2min(s, s1)
            t2ma = t2max(s, s1)

            integ = vegas.Integrator([[t2mi, t2ma], [0, 2 * np.pi]])
            result = integ(KSQ, nitn=10, neval=1000)

            Kfile.write("{:04.3f}   {:04.3f}   {:07.3f}\n".format(np.sqrt(s1), costh, result.mean))
    Kfile.close()

# KSQcosthplot(3.0**2)
fullt2plot()
