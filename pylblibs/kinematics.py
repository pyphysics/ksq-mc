conv=389.38

def lbd(x, y, z):
    return x ** 2 + y ** 2 + z ** 2 - 2 * x * y - 2 * x * z - 2 * y * z


def scal_prod_4d(v1, v2):
    val = v1[0] * v2[0]
    for i in range(1, 4):
        val = val - v1[i] * v2[i]

    return val


def v4tov3(v4):
    return v4[1:4]
