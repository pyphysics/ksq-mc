import scipy.special as sp
import numpy as np

def safegamma(x,eps=0.01):
    intx=int(round(x))
    if abs(intx-x)<eps:
        return 0.5*(sp.gamma(intx-eps)+sp.gamma(intx+eps))

    return sp.gamma(x)

def safe1F1(a,b,x,eps=0.01):
    inta=int(round(a))
    intb=int(round(b))
    if abs(inta-a)<eps:
        return 0.5*(sp.hyp1f1(inta-eps,b,x)+sp.hyp1f1(inta+eps,b,x))
    if abs(intb-b)<eps:
        return 0.5*(sp.hyp1f1(a,intb-eps,x)+sp.hyp1f1(a,intb+eps,x))

    return sp.hyp1f1(a,b,x)

def c1F1(a, b, x):
    # ===================================================
    # Purpose: Compute confluent hypergeometric function
    #          1F1(a,b,x)
    # Input  : a  --- Parameter
    #          b  --- Parameter ( b <> 0,-1,-2,... )
    #          x  --- Argument
    # Return:  HG --- M(a,b,x)
    # Routine called: scipy.special.gamma for computing â(x)
    # ===================================================
    pi = np.pi
    a0 = a
    a1 = a
    x0 = x
    hg = 0.0
    if b == 0.0 or b == -abs(int(b)):
        hg = 10.0 ** 300
    elif a == 0.0 or x == 0.0:
        hg = 1.0
    elif a == -1.0:
        hg = 1.0 - x / b
    elif a == b:
        hg = sp.exp(x)
    elif a - b == 1.0:
        hg = (1.0 + x / b) * sp.exp(x)
    elif a == 1.0 and b == 2.0:
        hg = (sp.exp(x) - 1.0) / x
    elif a == int(a) and a < 0.0:
        m = int(-a)
        r = 1.0
        hg = 1.0
        for k in range(1, m + 1):
            r = r * (a + k - 1.0) / k / (b + k - 1.0) * x
            hg = hg + r

    if not hg == 0.0:
        return hg

    if x < 0.0:
        a = b - a
        a0 = a
        x = abs(x)

    nl = 0
    if a < 2.0:
        nl = 0
    if a >= 2.0:
        nl = 1
        la = int(a)
        a = a - la - 1.0

    for n in range(0, nl + 1):
        if a0 >= 2.0:
            a = a + 1.0

        if x <= 30.0 + abs(b) or a < 0.0:
            hg = 1.0
            rg = 1.0
            for j in range(1, 501):
                rg = rg * (a + j - 1.0) / (j * (b + j - 1.0)) * x
                hg = hg + rg
                if abs(rg / hg) < 10.0 ** -15:
                    break
        else:
            ta = sp.gamma(a)
            tb = sp.gamma(b)
            xg = b - a
            tba = sp.gamma(xg)
            sum1 = 1.0
            sum2 = 1.0
            r1 = 1.0
            r2 = 1.0
            for ii in range(1, 8 + 1):
                r1 = -r1 * (a + ii - 1.0) * (a - b + ii) / (x * ii)
                r2 = -r2 * (b - a + ii - 1.0) * (a - ii) / (x * ii)
                sum1 = sum1 + r1
                sum2 = sum2 + r2

            hg1 = tb / tba * x ** (-a) * np.cos(pi * a) * sum1
            hg2 = tb / ta * sp.exp(x) * x ** (a - b) * sum2
            hg = hg1 + hg2

        if n == 0:
            y0 = hg
        if n == 1:
            y1 = hg

    if a0 >= 2.0:
        for i in range(1, la):
            hg = ((2.0 * a - b + x) * y1 + (b - a) * y0) / a
            y0 = y1
            y1 = hg
            a = a + 1.0

    if x0 < 0.0:
        hg = hg * sp.exp(x0)

    # a = a1
    # x = x0
    return hg

class SeriesDoesNotConvergeException(Exception):
    def __init__(self):
        super(SeriesDoesNotConvergeException,self).__init__("Hypergeometric series does not converge for this papameter value.")

def hyp1F1(a,b,x):
    if abs(x)>=1:
        print("x=",x)
        raise SeriesDoesNotConvergeException()

    a=complex(a)
    b=complex(b)
    val=complex(0)
    for i in range(0,10):
        val+=sp.gamma(a+i)*sp.gamma(b)/(sp.gamma(a)*sp.gamma(b+i))*x**i/sp.gamma(i+1)

    return val
