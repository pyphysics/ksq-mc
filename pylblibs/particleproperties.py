import numpy as np
m_proton=0.93827
m_pion=0.13957
m_eta=0.54786
m_etaprim=0.95778
m_ro=0.77526
m_a2=1.3183

gamma_ropipi=0.1491
gamma_a2pieta=0.0155
g_ropipi=np.sqrt(6*np.pi*gamma_ropipi*m_ro**2/np.sqrt(m_ro**2/4-m_pion**2)**3)
