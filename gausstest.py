import numpy as np
import scipy.integrate as integr
def fun(x):
    return np.exp(-x**2)

val=integr.quadrature(fun,-10,10)
print(val[0]**2)

