from pylblibs.particleproperties import m_pion as mpi
from pylblibs.particleproperties import m_eta as meta
import numpy as np
from pylblibs.kinematics import *

s = 20.0
t = -0.5

def energy(p,m):
    return np.sqrt(np.linalg.norm(p)**2+m**2)

pmod = np.sqrt(lbd(s, mpi ** 2, meta ** 2) / (4 * s))
costh = 1 + t / (2 * pmod ** 2)
sinth = np.sqrt(1 - costh ** 2)
p13v = pmod * np.array([0, 0, 1])
p23v = -p13v
p14v = np.concatenate(([energy(p13v,mpi)], p13v))
p24v = np.concatenate(([energy(p23v,meta)], p23v))
p33v = pmod * np.array([sinth, 0, costh])
p43v = -p33v
p34v = np.concatenate(([energy(p33v,mpi)], p33v))
p44v = np.concatenate(([energy(p43v,meta)], p43v))

u = scal_prod_4d(p14v - p44v, p14v - p44v)

# print(2.0 * (meta ** 2 + mpi ** 2))
# print(s + t + u)

u0=(meta**2-mpi**2)**2/s

z=-1-(u-u0)/(2*pmod**2)

print(costh)
print(z)
